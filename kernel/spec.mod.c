#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x44abaccb, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x1f0e19e7, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xb6b46a7c, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0xd0d8621b, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x1b63e4e7, __VMLINUX_SYMBOL_STR(pci_disable_device) },
	{ 0xf087137d, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xa4796ec5, __VMLINUX_SYMBOL_STR(fmc_device_register) },
	{ 0x7d11c268, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x68dfc59f, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x5d41c87c, __VMLINUX_SYMBOL_STR(param_ops_charp) },
	{ 0x9888874f, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0x2d2ed769, __VMLINUX_SYMBOL_STR(fmc_free_sdb_tree) },
	{ 0xb5c49069, __VMLINUX_SYMBOL_STR(fmc_device_unregister) },
	{ 0x98ff5863, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x50eedeb8, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xb4390f9a, __VMLINUX_SYMBOL_STR(mcount) },
	{ 0x6f6657c7, __VMLINUX_SYMBOL_STR(fmc_show_sdb_tree) },
	{ 0x2072ee9b, __VMLINUX_SYMBOL_STR(request_threaded_irq) },
	{ 0x2f80bcd2, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x42c8de35, __VMLINUX_SYMBOL_STR(ioremap_nocache) },
	{ 0xfc339389, __VMLINUX_SYMBOL_STR(put_device) },
	{ 0x3bd1b1f6, __VMLINUX_SYMBOL_STR(msecs_to_jiffies) },
	{ 0x442fe2e2, __VMLINUX_SYMBOL_STR(pci_enable_msi_range) },
	{ 0x6bb8914, __VMLINUX_SYMBOL_STR(pci_unregister_driver) },
	{ 0x5daad700, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0x8992c7ab, __VMLINUX_SYMBOL_STR(get_device) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x4a619f83, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x20b2bb2d, __VMLINUX_SYMBOL_STR(param_array_ops) },
	{ 0xff8e3f13, __VMLINUX_SYMBOL_STR(pci_disable_msi) },
	{ 0xedc03953, __VMLINUX_SYMBOL_STR(iounmap) },
	{ 0xdad17e64, __VMLINUX_SYMBOL_STR(__pci_register_driver) },
	{ 0x4af9d499, __VMLINUX_SYMBOL_STR(request_firmware) },
	{ 0x26c9f512, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x19a9e62b, __VMLINUX_SYMBOL_STR(complete) },
	{ 0x83e8467, __VMLINUX_SYMBOL_STR(fmc_scan_sdb_tree) },
	{ 0x381a54ba, __VMLINUX_SYMBOL_STR(pci_enable_device) },
	{ 0xb1d9523e, __VMLINUX_SYMBOL_STR(wait_for_completion_timeout) },
	{ 0x9301e86b, __VMLINUX_SYMBOL_STR(release_firmware) },
	{ 0xf20dabd8, __VMLINUX_SYMBOL_STR(free_irq) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=fmc";

MODULE_ALIAS("pci:v000010DCd0000018Dsv*sd*bc*sc*i*");
MODULE_ALIAS("pci:v00001A39d00000004sv*sd*bc*sc*i*");

MODULE_INFO(srcversion, "E1C1D1255390C9425449C90");
