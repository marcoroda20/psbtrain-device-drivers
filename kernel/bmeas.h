/*
 * Copyright CERN 2013
 * Author: Daniel Oberson
 *
 * Header for the mezzanine 
 *
 * bmeas.h
 */

#ifndef _BMEAS_H_
#define _BMEAS_H_

#include <linux/scatterlist.h>
#include <linux/fmc.h>
#include <linux/miscdevice.h>

extern uint32_t fmc_readl(struct fmc_device *fmc, int offset);
extern void fmc_writel(struct fmc_device *fmc, uint32_t val,int off);

struct bmeas_dev {
	/*struct fmc_device       *fmc;
	struct fmc_device       fmc_dev;
	struct device           dev;*/
	struct list_head        list;
	struct fmc_device       *fmc;
	struct miscdevice       misc;
	/* DMA */ //doberson
	unsigned char           enable_dma;	/* Flag 0xA5 if DMA is enabled */
	unsigned char           force_dma;	/* Flag to say that the DMA is forced */
	unsigned char           block_nber;	/* block number */
	unsigned int		*dma_buffer;	/* Pointer to DMA buffer */
	unsigned int		dma_buffer_len;	/* Length of DMA buffer */
	unsigned int            dmatag;         /* tag of cycle */
	struct sg_table		sgt;		/* Scatter list table for DMA */
	struct bmeasd_dma_item	*items;		/* Table of DMA items */
	dma_addr_t		dma_list_item;	/* Adress of list item */
	unsigned int		n_dma_err;	/* Number useful to check DMA error */
	ktime_t			time_tag[2];	/* Time value useful to measure duration */
};

//#define BMEAS_GATEWARE_DEFAULT_NAME "fmc/adc-100m14b.bin"
//extern int enable_auto_start;

/*
 * bmeasd_dma_item: The information about a DMA transfer
 * @start_addr:    pointer where start to retrieve data from device memory
 * @dma_addr_l:    low 32bit of the dma address on host memory
 * @dma_addr_h:    high 32bit of the dma address on host memory
 * @dma_len:       number of bytes to transfer from device to host
 * @next_addr_l:   low 32bit of the address of the next memory area to use
 * @next_addr_h:   high 32bit of the address of the next memory area to use
 * @attribute:     dma information about data transferm. At the moment it is used
 *                 only to provide the "last item" bit, direction is fixed to
 *                 device->host
 */
struct bmeasd_dma_item {
	uint32_t start_addr;	/* 0x00 */
	uint32_t dma_addr_l;	/* 0x04 */
	uint32_t dma_addr_h;	/* 0x08 */
	uint32_t dma_len;	/* 0x0C */
	uint32_t next_addr_l;	/* 0x10 */
	uint32_t next_addr_h;	/* 0x14 */
	uint32_t attribute;	/* 0x18 */
	uint32_t reserved;	/* ouch */
};

/* ADC register offset */
#define BMEAS_DMA_MEM_OFF	0x01000
#define BMEAS_ONEWIRE_MEM_OFF	0x01100
#define BMEAS_SPEC_CSR_MEM_OFF	0x01200
#define BMEAS_IRQ_MEM_OFF	0x02000
#define FMC_BMEAS_MEM_OFF	0x05000
#define FMC_BMEAS_I2C_MEM_OFF	0x06000
#define FMC_BMEAS_OWI_MEM_OFF	0X06100 /* one-wire */

/* DMA constant */
#define DEFAULT_DMA_DATA_LENGTH 900000//900000
#define SIZE_DMA_DATA           4//16//4

/* IRQ */
#define NO_IRQ	        0
#define END_OF_ACQ_IRQ	4

/* number maximum of bmeas */
#define MAX_BMEAS_DEVICES 4

/**
 * @brief  user argument  for ioctl BMEAS	
 * defining address, data to get in return
 */
struct bmeasd_reg {
	uint32_t addr; // address of the register, or base offset 
	uint32_t data; // value of read register 
};                             

struct bmeasd_dma {
	unsigned long size; // size of DMA 
};     

struct bmeasd_dma_tag {
	unsigned int dma_tag; // DMA tag
};     

struct bmeasd_cycle {
	unsigned int *pdata; // pointer to cycle values
	unsigned long size;  // size of cycle 
};     

struct firmware {
	char *frmw_name;  // firmware file name  
	unsigned int bus; // bus number of the spec
};     


// IOCTLS for this driver *
#define BMEAS_IOCMAGIC     'r'
#define	BMEAS_IOC_READ_REG   _IOWR(BMEAS_IOCMAGIC, 1, struct bmeasd_reg)
#define	BMEAS_IOC_WRITE_REG  _IOWR(BMEAS_IOCMAGIC, 2, struct bmeasd_reg)
#define	BMEAS_IOC_CFG_DMA    _IOWR(BMEAS_IOCMAGIC, 3, struct bmeasd_dma)
#define	BMEAS_IOC_START_DMA  _IO  (BMEAS_IOCMAGIC, 4)
//#define	BMEAS_IOC_START_DMA  _IOWR(BMEAS_IOCMAGIC, 4, struct bmeasd_dma)
#define	BMEAS_IOC_STOP_DMA   _IO  (BMEAS_IOCMAGIC, 5)
#define	BMEAS_IOC_LAST_CYCLE _IOWR(BMEAS_IOCMAGIC, 6, struct bmeasd_cycle)//struct cycle)
#define	BMEAS_IOC_DMA_TAG    _IOWR(BMEAS_IOCMAGIC, 7, struct bmeasd_dma_tag)
#define	BMEAS_IOC_FORCE_DMA  _IOWR(BMEAS_IOCMAGIC, 8, struct bmeasd_dma_tag)
#define	BMEAS_IOC_LOAD_FRMW  _IOR (BMEAS_IOCMAGIC, 9, struct firmware)


/* FIELD */
/*
 * bmeas_field_desc is a field register descriptor. By using address, mask
 * and shift the driver can describe every fields in registers.
 */
struct bmeas_field_desc {
	unsigned long addr; /* address of the register, or base offset */
	uint32_t mask; /* bit mask a register field */
	uint32_t shift; /* shift of the mask into the register */
};

/* All possible interrupt available */
enum bmeas_irq {
	BMEAS_NONE =      0x0,
	BMEAS_DMA_DONE =  0x1,
	BMEAS_DMA_ERR =   0x2,
	BMEAS_ACQ_END_0 = 0x4,
	BMEAS_ACQ_END_1 = 0x8,
	BMEAS_ACQ_END   = BMEAS_ACQ_END_0 | BMEAS_ACQ_END_1,
	BMEAS_ALL =       BMEAS_ACQ_END | BMEAS_DMA_DONE | BMEAS_DMA_ERR,
};

/* Device registers */
enum bmeas_regs_enum {
	/* DMA */
	BMEAS_DMA_CTL_SWP,
	BMEAS_DMA_CTL_ABORT,
	BMEAS_DMA_CTL_START,
	BMEAS_DMA_STA,
	BMEAS_DMA_ADDR,
	BMEAS_DMA_ADDR_L,
	BMEAS_DMA_ADDR_H,
	BMEAS_DMA_LEN,
	BMEAS_DMA_NEXT_L,
	BMEAS_DMA_NEXT_H,
	BMEAS_DMA_BR_DIR,
	BMEAS_DMA_BR_LAST,
	/* IRQ */
	BMEAS_IRQ_MASK,
	BMEAS_IRQ_SRC,
	BMEAS_IRQ_MULTI,
};
/* Definition of the bmeas registers address */
extern const struct bmeas_field_desc bmeas_regs[];
/*const struct bmeas_field_desc bmeas_regs[] = {
	// DMA 
	[BMEAS_DMA_CTL_SWP] =	{BMEAS_DMA_MEM_OFF + 0x00, 0x0003, 2},
	[BMEAS_DMA_CTL_ABORT] =	{BMEAS_DMA_MEM_OFF + 0x00, 0x0001, 1},
	[BMEAS_DMA_CTL_START] =	{BMEAS_DMA_MEM_OFF + 0x00, 0x0001, 0},
	[BMEAS_DMA_STA] =	{BMEAS_DMA_MEM_OFF + 0x04, 0x0007, 0},
	[BMEAS_DMA_ADDR] =	{BMEAS_DMA_MEM_OFF + 0x08, 0xFFFFFFFF, 0},
	[BMEAS_DMA_ADDR_L] =	{BMEAS_DMA_MEM_OFF + 0x0C, 0xFFFFFFFF, 0},
	[BMEAS_DMA_ADDR_H] =	{BMEAS_DMA_MEM_OFF + 0x10, 0xFFFFFFFF, 0},
	[BMEAS_DMA_LEN] =	{BMEAS_DMA_MEM_OFF + 0x14, 0xFFFFFFFF, 0},
	[BMEAS_DMA_NEXT_L] =	{BMEAS_DMA_MEM_OFF + 0x18, 0xFFFFFFFF, 0},
	[BMEAS_DMA_NEXT_H] =	{BMEAS_DMA_MEM_OFF + 0x1C, 0xFFFFFFFF, 0},
	[BMEAS_DMA_BR_DIR] =	{BMEAS_DMA_MEM_OFF + 0x20, 0x0001, 1},
	[BMEAS_DMA_BR_LAST] =	{BMEAS_DMA_MEM_OFF + 0x20, 0x0001, 0},
	// IRQ 
	[BMEAS_IRQ_MULTI] =	{BMEAS_IRQ_MEM_OFF + 0x00, 0x000F, 0},
	[BMEAS_IRQ_SRC] =	{BMEAS_IRQ_MEM_OFF + 0x04, 0x000F, 0},
	[BMEAS_IRQ_MASK] =	{BMEAS_IRQ_MEM_OFF + 0x08, 0x000F, 0},
};*/

// Registers for GN4124 access 
enum {
	// page 106 
	GNPPCI_MSI_CONTROL	= 0x48,		// actually, 3 smaller regs 
	GNPPCI_MSI_ADDRESS_LOW	= 0x4c,
	GNPPCI_MSI_ADDRESS_HIGH	= 0x50,
	GNPPCI_MSI_DATA		= 0x54,

	GNPCI_SYS_CFG_SYSTEM	= 0x800,

	// page 130 ff 
	GNINT_CTRL		= 0x810,
	GNINT_STAT		= 0x814,
	GNINT_CFG_0		= 0x820,
	GNINT_CFG_1		= 0x824,
	GNINT_CFG_2		= 0x828,
	GNINT_CFG_3		= 0x82c,
	GNINT_CFG_4		= 0x830,
	GNINT_CFG_5		= 0x834,
	GNINT_CFG_6		= 0x838,
	GNINT_CFG_7		= 0x83c,
#define GNINT_CFG(x) (GNINT_CFG_0 + 4 * (x))

	// page 146 ff 
	GNGPIO_BASE = 0xA00,
	GNGPIO_BYPASS_MODE	= GNGPIO_BASE,
	GNGPIO_DIRECTION_MODE	= GNGPIO_BASE + 0x04, // 0 == output 
	GNGPIO_OUTPUT_ENABLE	= GNGPIO_BASE + 0x08,
	GNGPIO_OUTPUT_VALUE	= GNGPIO_BASE + 0x0C,
	GNGPIO_INPUT_VALUE	= GNGPIO_BASE + 0x10,
	GNGPIO_INT_MASK		= GNGPIO_BASE + 0x14, // 1 == disabled 
	GNGPIO_INT_MASK_CLR	= GNGPIO_BASE + 0x18, // irq enable 
	GNGPIO_INT_MASK_SET	= GNGPIO_BASE + 0x1C, // irq disable 
	GNGPIO_INT_STATUS	= GNGPIO_BASE + 0x20,
	GNGPIO_INT_TYPE		= GNGPIO_BASE + 0x24, // 1 == level 
	GNGPIO_INT_VALUE	= GNGPIO_BASE + 0x28, // 1 == high/rise 
	GNGPIO_INT_ON_ANY	= GNGPIO_BASE + 0x2C, // both edges 

	// page 158 ff 
	FCL_BASE		= 0xB00,
	FCL_CTRL		= FCL_BASE,
	FCL_STATUS		= FCL_BASE + 0x04,
	FCL_IODATA_IN		= FCL_BASE + 0x08,
	FCL_IODATA_OUT		= FCL_BASE + 0x0C,
	FCL_EN			= FCL_BASE + 0x10,
	FCL_TIMER_0		= FCL_BASE + 0x14,
	FCL_TIMER_1		= FCL_BASE + 0x18,
	FCL_CLK_DIV		= FCL_BASE + 0x1C,
	FCL_IRQ			= FCL_BASE + 0x20,
	FCL_TIMER_CTRL		= FCL_BASE + 0x24,
	FCL_IM			= FCL_BASE + 0x28,
	FCL_TIMER2_0		= FCL_BASE + 0x2C,
	FCL_TIMER2_1		= FCL_BASE + 0x30,
	FCL_DBG_STS		= FCL_BASE + 0x34,

	FCL_FIFO		= 0xE00,

	PCI_SYS_CFG_SYSTEM	= 0x800
};


/*
 * Get a field from a register value. You read a register from your device,
 * then you use this function to get a filed defined with bmeas_field_desc
 * from the read value
 */
static inline uint32_t bmeas_get_field(const struct bmeas_field_desc *fld,
				     uint32_t fld_val)
{
	return (fld_val & (fld->mask << fld->shift)) >> fld->shift;
}
/*
 * Set a field to a register value. You read a register from your device,
 * then you use this function to set a field defined with bmeas_field_desc
 * into the read value. Then you can write the register
 */
static inline uint32_t bmeas_set_field(const struct bmeas_field_desc *fld,
				     uint32_t fld_val, uint32_t usr_val)
{
	return (fld_val & (~(fld->mask << fld->shift))) |
	       (usr_val << fld->shift);
}

#ifdef __KERNEL__ /* All the rest is only of kernel users */

/* Hardware filed-based access */
static inline int bmeas_hardware_write(struct fmc_device *fmc,
				      enum bmeas_regs_enum index,
				      uint32_t usr_val)
{
	uint32_t cur, val;

	if ((usr_val & (~bmeas_regs[index].mask))) {
                printk(KERN_INFO "value 0x%x must fit mask 0x%x\n", usr_val,bmeas_regs[index].mask);
		return -EINVAL;
	}
	// Read current register
	cur = fmc_readl(fmc, bmeas_regs[index].addr);
	val = bmeas_set_field(&bmeas_regs[index], cur, usr_val);

	fmc_writel(fmc, val, bmeas_regs[index].addr);
	return 0;
}
static inline int bmeas_hardware_write32(struct fmc_device *fmc,
				      uint32_t addr,
				      uint32_t usr_val)
{
	/* Write current register */
	fmc_writel(fmc, usr_val, addr);
	return 0;
}

static inline void bmeas_hardware_read(struct fmc_device *fmc,
				       enum bmeas_regs_enum index,
				       uint32_t *usr_val)
{
	uint32_t cur;

	/* Read current register */
	cur = fmc_readl(fmc, bmeas_regs[index].addr);
	/* Return the value */
	*usr_val = bmeas_get_field(&bmeas_regs[index], cur);
}

static inline void bmeas_hardware_read32(struct fmc_device *fmc,
				       uint32_t addr,
				       uint32_t *usr_val)
{
	/* Return the value */
	*usr_val = fmc_readl(fmc, addr);
}

/* DMA functions */
extern void cfg_start_dma(struct bmeas_dev *bmeasdev);
extern int bmeas_map_dma(struct bmeas_dev *bmeasdev, unsigned int n_data,
					unsigned int n_bytes_data);
extern void bmeas_unmap_dma(struct bmeas_dev *bmeasdev);
extern void bmeas_dma_done(struct bmeas_dev *bmeasdev);

#endif /* __KERNEL__ */
#endif /*  BMEAS_H_ */
