/*
 * Copyright CERN 2013
 * Author: Daniel Oberson
 *
 * Table of register masks, used by driver functions
 */

#include "bmeas.h"

/* Definition of the bmeas registers address */
const struct bmeas_field_desc bmeas_regs[] = {
	/* DMA */
	[BMEAS_DMA_CTL_SWP] =	{BMEAS_DMA_MEM_OFF + 0x00, 0x0003, 2},
	[BMEAS_DMA_CTL_ABORT] =	{BMEAS_DMA_MEM_OFF + 0x00, 0x0001, 1},
	[BMEAS_DMA_CTL_START] =	{BMEAS_DMA_MEM_OFF + 0x00, 0x0001, 0},
	[BMEAS_DMA_STA] =	{BMEAS_DMA_MEM_OFF + 0x04, 0x0007, 0},
	[BMEAS_DMA_ADDR] =	{BMEAS_DMA_MEM_OFF + 0x08, 0xFFFFFFFF, 0},
	[BMEAS_DMA_ADDR_L] =	{BMEAS_DMA_MEM_OFF + 0x0C, 0xFFFFFFFF, 0},
	[BMEAS_DMA_ADDR_H] =	{BMEAS_DMA_MEM_OFF + 0x10, 0xFFFFFFFF, 0},
	[BMEAS_DMA_LEN] =	{BMEAS_DMA_MEM_OFF + 0x14, 0xFFFFFFFF, 0},
	[BMEAS_DMA_NEXT_L] =	{BMEAS_DMA_MEM_OFF + 0x18, 0xFFFFFFFF, 0},
	[BMEAS_DMA_NEXT_H] =	{BMEAS_DMA_MEM_OFF + 0x1C, 0xFFFFFFFF, 0},
	[BMEAS_DMA_BR_DIR] =	{BMEAS_DMA_MEM_OFF + 0x20, 0x0001, 1},
	[BMEAS_DMA_BR_LAST] =	{BMEAS_DMA_MEM_OFF + 0x20, 0x0001, 0},
	/* IRQ */
	[BMEAS_IRQ_MULTI] =	{BMEAS_IRQ_MEM_OFF + 0x00, 0x000F, 0},
	[BMEAS_IRQ_SRC] =	{BMEAS_IRQ_MEM_OFF + 0x04, 0x000F, 0},
	[BMEAS_IRQ_MASK] =	{BMEAS_IRQ_MEM_OFF + 0x08, 0x000F, 0},
};

