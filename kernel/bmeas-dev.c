/*
 * Copyright (C) 2013 CERN (www.cern.ch)
 * Author: Daniel Oberson
 *
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>
#include <linux/fmc.h>
#include <linux/ktime.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#include <linux/cdev.h>

#include "bmeas.h"

static LIST_HEAD(bmeasd_devices);
static DEFINE_SPINLOCK(bmeasd_lock);

static struct fmc_driver bmeas_drv; 
irqreturn_t bmeas_handler(int irq, void *dev_id);

static struct fmc_gpio t_gpio[] = {
	{
		.gpio = FMC_GPIO_IRQ(0),
		.mode = GPIOF_DIR_IN,
		.irqmode = IRQF_TRIGGER_RISING,
	}, {
		.gpio = FMC_GPIO_IRQ(1),
		.mode = GPIOF_DIR_IN,
		.irqmode = IRQF_TRIGGER_RISING,
	}
};

/*
 * get_irq_status
 * @fmc: pointer to fmc device
 * @irq_status: destination of irq status
 * @irq_multi: destination of irq multi
 *
 * Get irq and clear the register. To clear an interrupt we have to write 1
 * on the handled interrupt. We handle all interrupt so we clear all interrupts
 */
static void get_irq_src(struct fmc_device *fmc,
				uint32_t *irq_status, uint32_t *irq_multi)
{
	uint32_t irq_ctrl_en_mask;
	// Get current interrupts status 
	bmeas_hardware_read(fmc, BMEAS_IRQ_SRC, irq_status);
	bmeas_hardware_read(fmc, BMEAS_IRQ_MASK, &irq_ctrl_en_mask);
	bmeas_hardware_read(fmc, BMEAS_IRQ_MULTI, irq_multi);
	*irq_status = *irq_status&irq_ctrl_en_mask;
	*irq_multi = *irq_multi&irq_ctrl_en_mask;

	// Clear current interrupts status 
	bmeas_hardware_write(fmc, BMEAS_IRQ_SRC, *irq_status);
	bmeas_hardware_write(fmc, BMEAS_IRQ_MULTI, *irq_multi);
}

//static irqreturn_t bmeas_handler(int irq, void *dev_id)
irqreturn_t bmeas_handler(int irq, void *dev_id)
{
	struct fmc_device *fmc = dev_id;
	struct bmeas_dev* bmeasdev;

	uint32_t irq_ctrl_src,irq_ctrl_multi;
	int err,length;
	int max_try = 10;	
	ktime_t stop;
	s32 delta;

	list_for_each_entry(bmeasdev, &bmeasd_devices, list)
		if (bmeasdev->fmc == fmc)
			break;
	if (bmeasdev->enable_dma==(unsigned char)0xA5) {
		// IRQ src 
		get_irq_src(fmc, &irq_ctrl_src, &irq_ctrl_multi);
		if (!irq_ctrl_src)
		{
			//printk(KERN_DEBUG "IRQ none :%04x\n",irq_ctrl_src);
			return IRQ_NONE;
		}

irq_handler:
		if (irq_ctrl_src & BMEAS_DMA_DONE) {		
			//printk(KERN_DEBUG "IRQ DMA done        : %04x\n",irq_ctrl_src);

			bmeas_hardware_write(fmc,BMEAS_IRQ_SRC,BMEAS_DMA_DONE);		
			bmeas_dma_done(bmeasdev);//show data to debug
			if (bmeasdev->force_dma==0)
			{	
				bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,BMEAS_ACQ_END);
			}
			else
			{
				bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,0);
				bmeasdev->force_dma=0;
			}
			bmeasdev->time_tag[2] = ktime_get();
			//stop = ktime_get();
			delta = ktime_to_us(ktime_sub(bmeasdev->time_tag[2],bmeasdev->time_tag[1]));
			//bmeasdev->time_tag[2] = bmeasdev->time_tag[1];
			//printk("Stop DMA time tag : %ld\n",get_seconds());
			//printk("Time between EndOfAcqu and EndOfDMA in us: %d\n",delta);

		}
		if (unlikely((irq_ctrl_src | irq_ctrl_multi) & BMEAS_DMA_ERR)) {
			//printk(KERN_DEBUG "IRQ DMA error       : %04x\n",irq_ctrl_src);

			//Clean IRQ source		
			bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,0);
			bmeas_hardware_write(fmc,BMEAS_IRQ_SRC,BMEAS_ALL);
				//BMEAS_ACQ_END_0 | BMEAS_ACQ_END_1 | BMEAS_DMA_ERR | BMEAS_DMA_DONE);
			//bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,BMEAS_ACQ_END_0 | BMEAS_ACQ_END_1);
			bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,0);
			//bmeas_unmap_dma(bmeasdev);
		}

		if ((irq_ctrl_src | irq_ctrl_multi) & (BMEAS_ACQ_END_0 | BMEAS_ACQ_END_1))
		{
			stop = ktime_get();
			//delta = ktime_to_ms(ktime_sub(stop,bmeasdev->time_tag));
			//printk("Time between EndOfAcqu and EndOfDMA in ms: %d\n",delta);
			//printk(KERN_DEBUG "IRQ ACQ end        : %04x\n",irq_ctrl_src);

			bmeasdev->time_tag[1] = ktime_get();
			//printk("Start DMA time tag : %ld\n",get_seconds());
			if ((irq_ctrl_src | irq_ctrl_multi) & BMEAS_ACQ_END_0) 
			{
				bmeasdev->block_nber=0;
				//bmeas_hardware_write(fmc,BMEAS_IRQ_SRC,BMEAS_ACQ_END_0);
			}
			if ((irq_ctrl_src | irq_ctrl_multi) & BMEAS_ACQ_END_1) 
			{
				bmeasdev->block_nber=1;
				//bmeas_hardware_write(fmc,BMEAS_IRQ_SRC,BMEAS_ACQ_END_1);

			}
			//printk(KERN_DEBUG "bmeasdev->block_nber = %d\n",(unsigned int)bmeasdev->block_nber);
			bmeas_hardware_write(fmc,BMEAS_IRQ_SRC,BMEAS_ACQ_END_0 | BMEAS_ACQ_END_1);
			bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,BMEAS_DMA_ERR | BMEAS_DMA_DONE);

			if ((bmeasdev->dma_buffer == NULL) & (bmeasdev->items == NULL))
			{
				length=(bmeasdev->dma_buffer_len)/SIZE_DMA_DATA;
				err = bmeas_map_dma(bmeasdev,length,SIZE_DMA_DATA); 
						//720000 is the number of data to transfer TODO: modify as configurable parameter
						//     4 is the number of bytes for one data

				if (err)
				{
					printk(KERN_DEBUG "Error map of DMA");
					bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,0);
					bmeas_unmap_dma(bmeasdev);
				}
				else
				{
					//printk(KERN_DEBUG "Start DMA\n");
					cfg_start_dma(bmeasdev);
					bmeas_hardware_write(fmc,BMEAS_DMA_CTL_START,1);
				}
			}
			else
			{
				//printk(KERN_DEBUG "Start DMA\n");
				cfg_start_dma(bmeasdev);
				bmeas_hardware_write(fmc,BMEAS_DMA_CTL_START,1);
			}
			bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,BMEAS_DMA_ERR | BMEAS_DMA_DONE);
		}
		fmc->op->irq_ack(fmc);

		// Read again the interrupt status. It can happen that an interrupt
		// is raised while we was processing a previous interrupt. If there
		// are new active interrupts, then handle them.
		// This handler cannot monopolize the processor, so it check for
		// new interrupts only 'max_try' times.

		get_irq_src(fmc, &irq_ctrl_src, &irq_ctrl_multi);
		if ((irq_ctrl_src || irq_ctrl_multi) && --max_try)
			goto irq_handler; // to test

		//stop = ktime_get();
		//delta = ktime_to_ns(ktime_sub(stop,start));
		//printk("Time elapsed in ns: %d\n",delta);

		return IRQ_HANDLED;
	}
	else
		return IRQ_NONE;
	

}


/* at open time, we must identify our device */
static int bmeasd_open(struct inode *ino, struct file *f)
{
	int ret=0;	
	struct fmc_device *fmc;
	struct bmeas_dev* bmeasdev;
	int minor = iminor(ino);

	//printk(KERN_DEBUG "bmeasd_open %d\n",minor);

	list_for_each_entry(bmeasdev, &bmeasd_devices, list)
		if (bmeasdev->misc.minor == minor)
			break;
	if (bmeasdev->misc.minor != minor)
		return -ENODEV;
	fmc = bmeasdev->fmc;
	if (try_module_get(fmc->owner) == 0)
		return -ENODEV;

	f->private_data = bmeasdev;

	return ret;
}

static long bmeasd_ioctl(struct file *filp,
		    unsigned int cmd,
		    unsigned long arg)
{
	int i;
	uint32_t data;
	int err,length;
	ktime_t start,now;
	struct bmeasd_reg reg, *regp = &reg;
	struct bmeasd_dma dma, *dmap = &dma;
	struct bmeasd_dma_tag dma_tag, *dma_tagp = &dma_tag;
	struct bmeasd_cycle cycle, *cyclep = &cycle;
	struct firmware firmware, *firmwarep = &firmware;

	struct bmeas_dev *bmeasdev = filp->private_data;
	//printk(KERN_DEBUG "IOCTL\n");

	switch (cmd) {
	case BMEAS_IOC_READ_REG:
		//printk(KERN_DEBUG "BMEAS_IOC_READ_REG\n");
		//copy the struct to kernel space
		if (copy_from_user(regp, (const void __user*)arg, sizeof(reg)))
		{
			printk(KERN_DEBUG "Error copy_from_user()\n");
			return -EINVAL;
		}
		//do the read
		bmeas_hardware_read32(bmeasdev->fmc,regp->addr,&regp->data);
		//copy the structure to user space
		if (copy_to_user((void __user *)arg, regp, sizeof(reg)))
		{
			printk(KERN_DEBUG "Error copy_to_user()\n");
			return -EINVAL;
		}
		return 0;
		break;

	case BMEAS_IOC_WRITE_REG:
		//printk(KERN_DEBUG "BMEAS_IOC_WRITE_REG\n");
		//copy the struct to kernel space
		if (copy_from_user(regp, (const void __user*)arg, sizeof(reg)))
		{
			printk(KERN_DEBUG "Error copy_from_user()\n");
			return -EINVAL;
		}
		//do the write
		bmeas_hardware_write32(bmeasdev->fmc,regp->addr,regp->data);
		//reread for the user
		bmeas_hardware_read32(bmeasdev->fmc,regp->addr,&regp->data);
		//copy the structure to user space
		if (copy_to_user((void __user *)arg, regp, sizeof(reg)))
		{
			printk(KERN_DEBUG "Error copy_to_user()\n");
			return -EINVAL;
		}
		return 0;
		break;

	case BMEAS_IOC_CFG_DMA:
		//printk(KERN_DEBUG "BMEAS_IOC_CFG_DMA\n");
		//config DMA
		return 0;
		break;
		
	case BMEAS_IOC_START_DMA:
		//printk(KERN_DEBUG "BMEAS_IOC_START_DMA %08x\n",BMEAS_IRQ_MASK);
		//Set the sample enable bit
		bmeas_hardware_read32(bmeasdev->fmc,0x5000,&data);
		data = data | 0x80;
		bmeas_hardware_write32(bmeasdev->fmc,0x5000,data);
		bmeas_hardware_read32(bmeasdev->fmc,0x5000,&data);
		//Clean interrupt register 
		bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_MULTI,BMEAS_ALL);
		bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_SRC,BMEAS_ALL);
		//Start interrupt acqu_end
		bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_MASK,BMEAS_ACQ_END);
		printk(KERN_DEBUG "bmeas DMA started (misc minor : %d)\n",bmeasdev->misc.minor);

		return 0;
		break;
		
	case BMEAS_IOC_STOP_DMA:
		//printk(KERN_DEBUG "BMEAS_IOC_STOP_DMA\n");
		//Stop all interrupt 
		bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_MASK,0);
		//Clean interrupt register 
		bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_MULTI,BMEAS_ALL);
		bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_SRC,BMEAS_ALL);
		printk(KERN_DEBUG "bmeas DMA stopped (misc minor : %d)\n",bmeasdev->misc.minor);
		return 0;
		break;

	case BMEAS_IOC_LAST_CYCLE:
		//printk(KERN_DEBUG "BMEAS_IOC_LAST_CYCLE\n");
		//copy the struct to kernel space
		if (copy_from_user(cyclep, (const void __user*)arg, sizeof(cycle)))
		{
			printk(KERN_DEBUG "Error copy_from_user()\n");
			return -EINVAL;
		}
		//do the copy
		for (i=0;i<cycle.size;i++)
		{
			*(cycle.pdata+i) = *(bmeasdev->dma_buffer+i);			
		}
		return 0;
		break;

	case BMEAS_IOC_DMA_TAG:
		//printk(KERN_DEBUG "BMEAS_IOC_DMA_TAG\n");
		//copy the struct to kernel space
		if (copy_from_user(dma_tagp, (const void __user*)arg, sizeof(dma_tag)))
		{
			printk(KERN_DEBUG "Error copy_from_user()\n");
			return -EINVAL;
		}
		//copy the structure to user space
		dma_tag.dma_tag = bmeasdev->dmatag;
		if (copy_to_user((void __user *)arg, dma_tagp, sizeof(dma_tag)))
		{
			printk(KERN_DEBUG "Error copy_to_user()\n");
			return -EINVAL;
		}
		return 0;
		break;


	case BMEAS_IOC_FORCE_DMA:
		printk("Start force DMA\n");
		start = ktime_get();
		//copy the struct to kernel space
		if (copy_from_user(dma_tagp, (const void __user*)arg, sizeof(dma_tag)))
		{
			printk(KERN_DEBUG "Error copy_from_user()\n");
			return -EINVAL;
		}

		//Set the sample enable bit
		bmeas_hardware_read32(bmeasdev->fmc,0x5000,&data);
		data = data | 0x80;
		bmeas_hardware_write32(bmeasdev->fmc,0x5000,data);
		bmeas_hardware_read32(bmeasdev->fmc,0x5000,&data);
		//Clean interrupt register 
		bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_MULTI,BMEAS_ALL);
		bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_SRC,BMEAS_ALL);

		dma_tag.dma_tag = bmeasdev->dmatag;
		//printk(KERN_DEBUG "dma_tag => %d\n",dma_tag.dma_tag);
		bmeasdev->force_dma = 1;
		if (bmeasdev->block_nber==1) 
		{
			bmeasdev->block_nber=0;
		}
		else
		{
			bmeasdev->block_nber=1;

		}
		
		//printk(KERN_DEBUG " *dma_buffer : %p\n",bmeasdev->dma_buffer);
		//printk(KERN_DEBUG " *bmeasdev   : %p\n",bmeasdev);
		if ((bmeasdev->dma_buffer == NULL) & (bmeasdev->items == NULL))
		{
			//printk(KERN_DEBUG "Config force DMA\n");
			length=(bmeasdev->dma_buffer_len)/SIZE_DMA_DATA;
			err = bmeas_map_dma(bmeasdev,length,SIZE_DMA_DATA); 
					//720000 is the number of data to transfer TODO: modify as configurable parameter
					//     4 is the number of bytes for one data
			printk(KERN_DEBUG "Error : %d\n",err);
			if (err)
			{
				//printk(KERN_DEBUG "Error map of DMA");
				bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_MASK,0);
				bmeas_unmap_dma(bmeasdev);
			}
			else
			{
				//printk(KERN_DEBUG "Map DMA and force DMA\n");
				cfg_start_dma(bmeasdev);
				bmeas_hardware_write(bmeasdev->fmc,BMEAS_DMA_CTL_START,1);
			}
		}
		else
		{
			//printk(KERN_DEBUG "Force DMA\n");
			cfg_start_dma(bmeasdev);
			bmeas_hardware_write(bmeasdev->fmc,BMEAS_DMA_CTL_START,1);
		}
		bmeas_hardware_write(bmeasdev->fmc,BMEAS_IRQ_MASK,BMEAS_DMA_ERR | BMEAS_DMA_DONE);
		
		//now = ktime_get();
		//printk(KERN_DEBUG " Start time : %lld\n Now time : %lld\n",ktime_to_ms(start),ktime_to_ms(now));

		if (copy_to_user((void __user *)arg, dma_tagp, sizeof(dma_tag)))
		{
			printk(KERN_DEBUG "Error copy_to_user()\n");
			return -EINVAL;
		}
		return 0;
		break;

	case BMEAS_IOC_LOAD_FRMW:
		//printk(KERN_DEBUG "BMEAS_IOC_LOAD_FRMW\n");
		//load firmware
		return 0;
		break;

	default:
		return -ENOTTY;
		break;
	}

	return 0;
}

static int bmeasd_release(struct inode *ino, struct file *f)
{
	struct fmc_device *fmc;
	struct bmeas_dev* bmeasdev = f->private_data;
	int minor = iminor(ino);

	list_for_each_entry(bmeasdev, &bmeasd_devices, list)
		if (bmeasdev->misc.minor == minor)
			break;
	fmc = bmeasdev->fmc;

	//printk(KERN_DEBUG "fmc : %08x\n",(unsigned int)bmeasdev->fmc);//TO CHECK
	//bmeasdev->enable_dma = (unsigned char)0x0;
	//module_put(fmc->owner);

	return 0;
}


//File operation
static const struct file_operations bmeasd_fops = {
	.owner = THIS_MODULE,
	.open = bmeasd_open,
	.unlocked_ioctl =bmeasd_ioctl,
	.release = bmeasd_release,
	//.llseek = generic_file_llseek,
	//.read = bmeasd_read,
	//.write = bmeasd_write,
};

static int bmeas_probe(struct fmc_device *fmc)
{
	int ret;
	int index = 0;
	struct bmeas_dev* bmeasdev;
	
	printk(KERN_DEBUG "Peakdetector Probe\n");
	if (fmc->op->validate)
	{
		index = fmc->op->validate(fmc, &bmeas_drv);
		printk("Peakdetector validate : 0x%x\n",index);
	}
	if (index < 0)
		return -EINVAL; /* not our device: invalid */

	/* ignore error code of call below, we really don't care */
	fmc->op->gpio_config(fmc, t_gpio, ARRAY_SIZE(t_gpio));

	/* Reprogram, if asked to. ESRCH == no filename specified */
	ret = -ESRCH;
	if (fmc->op->reprogram)
		ret = fmc->op->reprogram(fmc, &bmeas_drv, "");
	if (ret == -ESRCH)
		ret = 0;

	// Char device part 
	bmeasdev = kzalloc(sizeof(*bmeasdev), GFP_KERNEL);
	if (!bmeasdev)
		return -ENOMEM;
	bmeasdev->fmc = fmc;
	bmeasdev->misc.minor = MISC_DYNAMIC_MINOR;
	bmeasdev->misc.fops = &bmeasd_fops;
	bmeasdev->misc.name = kstrdup(dev_name(&fmc->dev), GFP_KERNEL);
	printk(KERN_DEBUG "bmeasdev->misc.name : %s and bmeasdev->misc.minor : %d \n",bmeasdev->misc.name,bmeasdev->misc.minor);

	spin_lock(&bmeasd_lock);
	ret = misc_register(&bmeasdev->misc);
	if (ret < 0)
		goto err_unlock;
	list_add(&bmeasdev->list, &bmeasd_devices);
	spin_unlock(&bmeasd_lock);

	dev_info(&bmeasdev->fmc->dev, "Created misc device \"%s\"\n",
		 bmeasdev->misc.name);

	//IRQ Handler register
	ret = fmc->op->irq_request(fmc, bmeas_handler, "fmc-bmeas", IRQF_SHARED);
	if (ret < 0)
		return ret;
	//DMA basic configuration 
	bmeasdev->dma_buffer_len = DEFAULT_DMA_DATA_LENGTH*SIZE_DMA_DATA;
	bmeasdev->force_dma = 0;
	bmeasdev->enable_dma = (unsigned char)0xA5;
	bmeasdev->dmatag = 0;

	//Clean enable interrupt register 
	bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,0);
	//Clean interrupt register 
	bmeas_hardware_write(fmc,BMEAS_IRQ_SRC,7);

	return ret;

err_unlock:
	spin_unlock(&bmeasd_lock);
	kfree(bmeasdev->misc.name);
	kfree(bmeasdev);
	return ret;
}

static int bmeas_remove(struct fmc_device *fmc)
{
	struct bmeas_dev* bmeasdev;

	printk("Peakdetector Remove\n");
	list_for_each_entry(bmeasdev, &bmeasd_devices, list)
		if (bmeasdev->fmc == fmc)
			break;
	//Disable interrupt on device
	bmeas_hardware_write(fmc,BMEAS_IRQ_MASK,0);
	fmc->op->irq_free(fmc);

	if (bmeasdev->fmc != fmc) {
		dev_err(&fmc->dev, "remove called but not found\n");
		return -ENODEV;
	}

	spin_lock(&bmeasd_lock);
	list_del(&bmeasdev->list);
	misc_deregister(&bmeasdev->misc);
	kfree(bmeasdev->misc.name);
	kfree(bmeasdev);
	spin_unlock(&bmeasd_lock);

	return 0;
}

static struct fmc_driver bmeas_drv = {
	.version = FMC_VERSION,
	.driver.name = KBUILD_MODNAME,
	.probe = bmeas_probe,
	.remove = bmeas_remove,
	/* no table, as the current match just matches everything */
};

 /* We accept the generic parameters */
FMC_PARAM_BUSID(bmeas_drv);
//FMC_PARAM_GATEWARE(bmeas_drv);

static int bmeas_init(void)
{
	int ret;
	printk("Peakdetector Init\n");
	ret = fmc_driver_register(&bmeas_drv);
	return ret;
}

static void bmeas_exit(void)
{
	printk("Peakdetector Exit\n");
	fmc_driver_unregister(&bmeas_drv);
}

module_init(bmeas_init);
module_exit(bmeas_exit);

MODULE_VERSION(GIT_VERSION);
MODULE_LICENSE("GPL"); /* public domain */
