#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "bmeas-lib.h"


uint32_t read_reg(int bus, uint32_t addr)
{
	int fd;
	struct bmeasd_reg read_reg;
	char filename[13];
	
	sprintf(filename,"/dev/fmc-0%d00",bus);
	//printf("%s\n",filename);

	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		printf("Error in opening file \n");
		exit(-1);
	}
	//printf("File opened \n");

	//printf("Argument : %s\n",cmd_READ);
	read_reg.addr = addr;
	//printf("Address = %08x\n",read_reg.addr);
	ioctl(fd,BMEAS_IOC_READ_REG,&read_reg);   
	//printf("BMEAS_IOC_READ_REG : %08x\n",BMEAS_IOC_READ_REG);
	//printf("READ register %08x => %08x\n",read_reg.addr,read_reg.data);

	close(fd);
	//printf("File closed \n");

	return read_reg.data;
}


uint32_t write_reg(int bus, uint32_t addr, uint32_t data)
{
	int fd;
	struct bmeasd_reg   write_reg;
	char filename[13];
	
	sprintf(filename,"/dev/fmc-0%d00",bus);
	//printf("%s\n",filename);

	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		printf("Error in opening file \n");
		exit(-1);
	}
	//printf("File opened \n");

	write_reg.addr = addr;
	//printf("Addresse = %08x\n",write_reg.addr);
	write_reg.data = data;
	//printf("Data     = %08x\n",write_reg.data);

	//printf("WRITE register %08x => %08x\n",write_reg.addr,write_reg.data);
	ioctl(fd,BMEAS_IOC_WRITE_REG,&write_reg);  
	//printf("after write reg. %08x => %08x\n",write_reg.addr,write_reg.data);

	close(fd);
	//printf("File closed \n");

	return write_reg.data;
}

void start_dma(int bus)
{
	int fd;
	char filename[13];
	
	sprintf(filename,"/dev/fmc-0%d00",bus);
	//printf("%s\n",filename);

	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		printf("Error in opening file \n");
		exit(-1);
	}
	//printf("File opened \n");

	//printf("Argument : %s\n",cmd_START_DMA);
	ioctl(fd,BMEAS_IOC_START_DMA);  //ioctl call
	
	close(fd);
	//printf("File closed \n");
}

void stop_dma(int bus)
{
	int fd;
	char filename[13];
	
	sprintf(filename,"/dev/fmc-0%d00",bus);
	//printf("%s\n",filename);

	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		printf("Error in opening file \n");
		exit(-1);
	}
	//printf("File opened \n");

	//printf("Argument : %s\n",cmd_STOP_DMA);
	ioctl(fd,BMEAS_IOC_STOP_DMA);  //ioctl call

	close(fd);
	//printf("File closed \n");
}

unsigned int dma_tag(int bus)
{
	int fd;
	struct bmeasd_dma_tag dma_tag;
	char filename[13];
	
	sprintf(filename,"/dev/fmc-0%d00",bus);
	
	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		printf("Error in opening file \n");
		exit(-1);
	}
	ioctl(fd,BMEAS_IOC_DMA_TAG,&dma_tag);  //ioctl call

	close(fd);

	return dma_tag.dma_tag;
}

void last_cycle(int bus, uint32_t *last_cycle_pointer, uint32_t *last_cycle_address)
{
	int fd,i;
	int32_t max,index_max;
	struct bmeasd_cycle last_cycle;
	char filename[13];
	
	last_cycle.pdata = last_cycle_pointer;
	//last_cycle.pdata = (unsigned int *)malloc(4*DMA_DATA_LENGTH*SIZE_DMA_DATA);
	//last_cycle.size = DMA_DATA_LENGTH;

	sprintf(filename,"/dev/fmc-0%d00",bus);
	//printf("%s\n",filename);

	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		printf("Error in opening file \n");
		exit(-1);
	}

	//read the adress of the last sample for the size of last cycle
	*last_cycle_address = read_reg(bus, 0x5010);	
	last_cycle.size = (*last_cycle_address)*4;
	//printf("last_sample : %d\n",*last_cycle_address);

	ioctl(fd,BMEAS_IOC_LAST_CYCLE,&last_cycle);  //ioctl call

	max=0;
	for (i=0;i<last_cycle.size/2;i++)
	{
		/*if ((i>=0)&(i<8)){
			printf("Buffer[%d] : 0x%08x \n",i,*((last_cycle.pdata)+i));
		}
		if (i>last_cycle.size-9){
			printf("Buffer[%d] : 0x%08x \n",i,*((last_cycle.pdata)+i));
		}*/
		if (i%4==2)
		{
			if (((int32_t)*((last_cycle.pdata)+i))>max) 
			{
				max = *((last_cycle.pdata)+i);
				index_max = i/4;
			}
		}
		
	}
	printf("Max B int32_t : %d with the index : %d \n",max,index_max);

	close(fd);
	//printf("File closed \n");
}

void force_dma(int bus)
{
	int fd;
	//struct bmeasd_dma_tag dma_tag;
	char filename[13];
	
	sprintf(filename,"/dev/fmc-0%d00",bus);
	
	fd = open(filename, O_RDWR);
	if (fd == -1)
	{
		printf("Error in opening file \n");
		exit(-1);
	}
	ioctl(fd,BMEAS_IOC_FORCE_DMA);  //ioctl call

	close(fd);
}


