/*
 * Copyright CERN 2014
 * Author: Daniel Oberson
 *
 * Header bmeas library
 *
 * bmeas-lib.h
 */


#ifndef _BMEAS_LIB_H_
#define _BMEAS_LIB_H_

#include <linux/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

//Test command
char cmd_BUS[] = "b";
char cmd_mBUS[] = "-b";
char cmd_READ[] = "r";
char cmd_WRITE[] = "w";
char cmd_START_DMA[] = "start_dma";
char cmd_STOP_DMA[] = "stop_dma";
char cmd_DMA_TAG[] = "dma_tag";
char cmd_LAST_CYCLE[] = "last_cycle";
char cmd_FORCE_DMA[] = "dma";
char cmd_TIMING[] = "timing";

/* Bus constant */
#define INTEGRAL_BUS_NO 3
#define PEAK_BUS_NO     4

/* DMA constant */
#define DEFAULT_DMA_DATA_LENGTH 900000//900000
#define DMA_DATA_LENGTH         DEFAULT_DMA_DATA_LENGTH//100000
#define SIZE_DMA_DATA           4//16//4

struct bmeasd_reg {
	uint32_t addr; // address of the register, or base offset 
	uint32_t data; // value of read register 
};                             

struct bmeasd_dma {
	unsigned long size; // size of DMA 
};     

struct bmeasd_dma_tag {
	unsigned int dma_tag; // DMA tag
};     

struct bmeasd_cycle {
	unsigned int *pdata; // pointer to cycle values
	unsigned long size;  // size of cycle 
};     

struct firmware {
	char *frmw_name;  // firmware file name  
	unsigned int bus; // bus number of the spec
};     

// IOCTLS cmd
#define BMEAS_IOCMAGIC     'r'
#define	BMEAS_IOC_READ_REG   _IOWR(BMEAS_IOCMAGIC, 1, struct bmeasd_reg)
#define	BMEAS_IOC_WRITE_REG  _IOWR(BMEAS_IOCMAGIC, 2, struct bmeasd_reg)
#define	BMEAS_IOC_CFG_DMA    _IOWR(BMEAS_IOCMAGIC, 3, struct bmeasd_dma)
#define	BMEAS_IOC_START_DMA  _IO  (BMEAS_IOCMAGIC, 4)
//#define	BMEAS_IOC_START_DMA  _IOWR(BMEAS_IOCMAGIC, 4, struct bmeasd_dma)
#define	BMEAS_IOC_STOP_DMA   _IO  (BMEAS_IOCMAGIC, 5)
#define	BMEAS_IOC_LAST_CYCLE _IOWR(BMEAS_IOCMAGIC, 6, struct bmeasd_cycle)//struct cycle)
#define	BMEAS_IOC_DMA_TAG    _IOWR(BMEAS_IOCMAGIC, 7, struct bmeasd_dma_tag)
#define	BMEAS_IOC_FORCE_DMA  _IOWR(BMEAS_IOCMAGIC, 8, struct bmeasd_dma_tag)
#define	BMEAS_IOC_LOAD_FRMW  _IOR (BMEAS_IOCMAGIC, 9, struct firmware)


#endif /*  BMEAS_LIB_H_ */
