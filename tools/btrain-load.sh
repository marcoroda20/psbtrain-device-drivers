#!/bin/bash
echo

DRIVER_MOUNTED=`lsmod | grep fmc | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver fmc.ko already inserted
else
 echo Insert driver fmc.ko...
 insmod /lib/modules/`uname -r`/extra/fmc.ko
 echo OK
fi
sleep 3

DRIVER_MOUNTED=`lsmod | grep spec | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver spec.ko already inserted
else
 echo Insert driver spec.ko...
 insmod /lib/modules/`uname -r`/extra/spec.ko
 echo OK
fi
sleep 3

echo Load firmware spec_top_fmc_adc2M18b2ch.bin... 
 ./spec-fwloader -b 5 ../../firmware/spec_top_fmc_adc2M18b2ch.bin
sleep 1
echo Load firmware spec_top_fmc_adc_16b_10Ms.bin ...
 ./spec-fwloader -b 6 ../../firmware/spec_top_fmc_adc_16b_10Ms.bin
sleep 1

DRIVER_MOUNTED=`lsmod | grep bmeas | wc -l`
if [ $DRIVER_MOUNTED -ne 0 ]
then
 echo Driver bmeas.ko already inserted
else
 echo Insert driver bmeas.ko...
 insmod /lib/modules/`uname -r`/extra/bmeas.ko
 echo OK
fi
sleep 2

echo " rm -f /dev/fmc-0500"
rm -f /dev/fmc-0500
echo " rm -f /dev/fmc-0600"
rm -f /dev/fmc-0600

minor_5=`awk -F " " 'BEGIN {} $2 = /fmc-0500/ { print $1 }' /proc/misc`
echo "Create mknod with correct minor number : mknod /dev/fmc-0500 c 10 $minor_5"
#awk -F " " 'BEGIN {} $2 = /fmc-0500/ { mknod /dev/fmc-0500 c 10 $1 }' /proc/misc
mknod /dev/fmc-0500 c 10 $minor_5
minor_6=`awk -F " " 'BEGIN {} $2 = /fmc-0600/ { print $1 }' /proc/misc`
echo "Create mknod with correct minor number : mknod /dev/fmc-0600 c 10 $minor_6"
#awk -F " " 'BEGIN {} $2 = /fmc-0600/ { mknod /dev/fmc-0600 c 10 $1 }' /proc/misc
mknod /dev/fmc-0600 c 10 $minor_6

echo "Give rights to user : chmod 666 /dev/fmc-0500"
chmod 666 /dev/fmc-0500
echo "Give rights to user : chmod 666 /dev/fmc-0600"
chmod 666 /dev/fmc-0600

